<?php

namespace Drupal\login_lock\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\login_lock\LoginLockHelperInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Login Lock status form.
 */
class StatusForm extends FormBase {

  /**
   * Login Lock helper.
   *
   * @var \Drupal\login_lock\LoginLockHelperInterface
   */
  protected $loginLockHelper;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\login_lock\LoginLockHelperInterface $loginLockHelper
   *   Login Lock helper.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(LoginLockHelperInterface $loginLockHelper, EntityTypeManagerInterface $entity_type_manager) {
    $this->loginLockHelper = $loginLockHelper;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('login_lock.helper'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_lock_status_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['login_lock_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Login lock'),
      '#description' => $this->t('If enabled login lock, users without permission to bypass login lock will be logged out.'),
      '#default_value' => $this->loginLockHelper->isLoginLockEnabled(),
    ];

    $roles_with_permission = [];
    /** @var \Drupal\user\RoleInterface[] $roles */
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    unset($roles[RoleInterface::ANONYMOUS_ID]);

    if ($roles) {
      foreach ($roles as $role) {
        if ($this->loginLockHelper->canRoleBypassLoginLock($role)) {
          $roles_with_permission[] = $role->label();
        }
      }
    }

    $form['roles'] = [
      [
        '#type' => 'item',
        '#plain_text' => $this->t('Here is a list of roles able to login even if enabled Login lock:'),
      ],
      [
        '#theme' => 'item_list',
        '#items' => $roles_with_permission,
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('login_lock_status'))) {
      $this->loginLockHelper->enableLock();
    }
    else {
      $this->loginLockHelper->disableLock();
    }
  }

}
