<?php

namespace Drupal\login_lock\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\State\StateInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Login Lock settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The state key-value collection.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The Drupal Path Validator service.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key-value collection to use.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The path alias manager service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    StateInterface $state,
    PathValidatorInterface $path_validator,
    AliasManagerInterface $alias_manager
  ) {
    parent::__construct($config_factory);
    $this->state = $state;
    $this->pathValidator = $path_validator;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('path.validator'),
      $container->get('path_alias.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_lock_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['login_lock.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('login_lock.settings');

    $form['redirect_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect url'),
      '#description' => $this->t('Drupal will redirect the user to this URL when a user will be logged out during enabled Login Lock.'),
      '#default_value' => $config->get('redirect_url'),
      '#required' => TRUE,
    ];

    $form['roles']['roles_bypass_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Roles able to bypass Login Lock'),
      '#required' => TRUE,
      '#options' => [
        'exclusive' => $this->t('All roles except the selected roles'),
        'inclusive' => $this->t('Only the selected roles'),
      ],
      '#default_value' => $config->get('roles_bypass_type'),
    ];

    $roles = Role::loadMultiple();
    unset($roles[RoleInterface::ANONYMOUS_ID]);
    unset($roles[RoleInterface::AUTHENTICATED_ID]);

    $roles_options = [];
    foreach ($roles as $role) {
      $roles_options[$role->id()] = $role->label();
    }

    $roles_default = $config->get('roles_bypass') ?? [];
    $form['roles']['roles_bypass'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Selected roles'),
      '#options' => $roles_options,
      '#default_value' => $roles_default,
    ];

    $form['login_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Login message'),
      '#default_value' => $config->get('login_message'),
      '#description' => $this->t('The message that will be displayed if the user tried to log in when the login lock is enabled, and this is a user who does not have permission to bypass this login lock.'),
      '#required' => TRUE,
    ];

    $form['logout_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Logout message'),
      '#default_value' => $config->get('logout_message'),
      '#description' => $this->t('The message that will appear when the user is unauthorized due to the enabled login lock.'),
    ];

    $form['status_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Status message'),
      '#default_value' => $config->get('status_message'),
      '#description' => $this->t('The message that will appear on admin routes when the user use website during enabled Login lock.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check for empty redirect path.
    if ($form_state->isValueEmpty('redirect_url')) {
      // Set to default "/".
      $form_state->setValueForElement($form['redirect_url'], '/');
    }
    else {
      // Get the normal path of the redirect.
      $form_state->setValueForElement($form['redirect_url'], $this->aliasManager->getPathByAlias($form_state->getValue('redirect_url')));
    }
    // Validate redirect path.
    if (($value = $form_state->getValue('redirect_url')) && $value[0] !== '/') {
      $form_state->setErrorByName('redirect_url', $this->t("The path '%path' has to start with a slash.", ['%path' => $form_state->getValue('redirect_url')]));

    }
    if (!$this->pathValidator->isValid($form_state->getValue('redirect_url'))) {
      $form_state->setErrorByName('redirect_url', $this->t("Either the path '%path' is invalid or you do not have access to it.", ['%path' => $form_state->getValue('redirect_url')]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('login_lock.settings');
    $config->set('login_message', $form_state->getValue('login_message'));
    $config->set('logout_message', $form_state->getValue('logout_message'));
    $config->set('status_message', $form_state->getValue('status_message'));
    $config->set('redirect_url', $form_state->getValue('redirect_url'));
    $config->set('roles_bypass_type', $form_state->getValue('roles_bypass_type'));
    $config->set('roles_bypass', $form_state->getValue('roles_bypass'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
