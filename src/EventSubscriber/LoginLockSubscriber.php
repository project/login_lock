<?php

namespace Drupal\login_lock\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\login_lock\LoginLockHelperInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Maintenance mode subscriber to log out users.
 */
class LoginLockSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The current account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * Login Lock helper.
   *
   * @var \Drupal\login_lock\LoginLockHelperInterface
   */
  protected $loginLockHelper;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new LoginLockSubscriber.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\login_lock\LoginLockHelperInterface $loginLockHelper
   *   Login Lock helper.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(AccountInterface $account, LoginLockHelperInterface $loginLockHelper, MessengerInterface $messenger) {
    $this->account = $account;
    $this->loginLockHelper = $loginLockHelper;
    $this->messenger = $messenger;
  }

  /**
   * Logout users if Login Lock enabled.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   */
  public function onKernelRequestLoginLock(RequestEvent $event) {
    $request = $event->getRequest();
    $route_match = RouteMatch::createFromRequest($request);

    // If enabled Login Lock log out unprivileged users.
    if ($this->account->isAuthenticated() && $this->loginLockHelper->isLoginLockEnabled() && !$this->loginLockHelper->canUserBypassLoginLock($this->account)) {
      user_logout();

      // Redirect to configured url.
      $redirect_url = $this->loginLockHelper->getRedirectUrl();

      // If configured logout message add query param "login_lock" for show
      // message after redirect.
      $logout_message = $this->loginLockHelper->getLogoutMessage();
      if (!empty($logout_message)) {
        $redirect_url->setOption('query', [
          'login_lock' => TRUE,
        ]);
      }

      $response = new RedirectResponse($redirect_url->toString());
      $event->setResponse($response);
    }
    elseif (!empty($event->getRequest()->get('login_lock')) && $this->account->isAnonymous()) {
      $logout_message = $this->loginLockHelper->getLogoutMessage();
      if (!empty($logout_message)) {
        $this->messenger->addStatus($logout_message);
      }

      // Redirect to configured url without param "login_lock".
      $redirect_url = $this->loginLockHelper->getRedirectUrl();
      $response = new RedirectResponse($redirect_url->toString());
      $event->setResponse($response);
    }

    if ($this->loginLockHelper->isLoginLockEnabled() && $this->loginLockHelper->canUserBypassLoginLock($this->account) && \Drupal::service('router.admin_context')->isAdminRoute($route_match->getRouteObject())) {
      $status_message = $this->loginLockHelper->getStatusMessage();
      if (!empty($status_message)) {
        $this->messenger->addWarning($status_message);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onKernelRequestLoginLock', 31];
    return $events;
  }

}
