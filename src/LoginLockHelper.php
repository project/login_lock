<?php

namespace Drupal\login_lock;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\user\RoleInterface;

/**
 * Login Lock helper.
 */
class LoginLockHelper implements LoginLockHelperInterface {

  /**
   * The state key-value collection.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * Constructs a \Drupal\login_lock\LoginLockHelper object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key-value collection to use.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   */
  public function __construct(StateInterface $state, ConfigFactoryInterface $configFactory) {
    $this->state = $state;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function isLoginLockEnabled(): bool {
    return $this->state->get('login_lock.status', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function canUserBypassLoginLock(AccountInterface $account): bool {
    $result = FALSE;

    // User with uid = 1 always can bypass login lock.
    if ($account->id() === '1') {
      return TRUE;
    }

    // Check if we should track the currently active user's role.
    $config = $this->configFactory->getEditable('login_lock.settings');
    $bypass_type = $config->get('roles_bypass_type');
    $stored_roles = $config->get('roles_bypass');

    $selected_roles = [];
    if ($stored_roles) {
      $selected_roles = array_filter($stored_roles);
    }

    // Compare the roles with current user.
    $union = array_intersect($selected_roles, $account->getRoles());
    // "inclusive" means add to the configured roles only, such that so long as
    // there is some overlap, we're in the clear.
    if ($bypass_type === 'inclusive' && !empty($union)) {
      $result = TRUE;
    }
    // "exclusive" means add to all roles except the ones configured, such that
    // so long as there is no overlap between current user and configured, we're
    // in the clear.
    elseif ($bypass_type === 'exclusive' && empty($union)) {
      $result = TRUE;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function canRoleBypassLoginLock(RoleInterface $role): bool {
    $result = FALSE;

    $config = $this->configFactory->getEditable('login_lock.settings');
    $bypass_type = $config->get('roles_bypass_type');
    $stored_roles = $config->get('roles_bypass');

    $selected_roles = [];
    if ($stored_roles) {
      $selected_roles = array_filter($stored_roles);
    }

    // Compare the roles with current user.
    $union = array_intersect($selected_roles, [$role->id()]);
    // "inclusive" means add to the configured roles only, such that so long as
    // there is some overlap, we're in the clear.
    if ($bypass_type === 'inclusive' && !empty($union)) {
      $result = TRUE;
    }
    // "exclusive" means add to all roles except the ones configured, such that
    // so long as there is no overlap between current user and configured, we're
    // in the clear.
    elseif ($bypass_type === 'exclusive' && empty($union)) {
      $result = TRUE;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function enableLock(): void {
    $this->state->set('login_lock.status', TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function disableLock(): void {
    $this->state->set('login_lock.status', FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUrl(): Url {
    $redirect_url = $this->configFactory->getEditable('login_lock.settings')->get('redirect_url');

    return Url::fromUserInput($redirect_url);
  }

  /**
   * {@inheritdoc}
   */
  public function getLoginMessage(): string {
    $login_message = $this->configFactory->getEditable('login_lock.settings')->get('login_message');

    return $login_message ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusMessage(): string {
    $status_message = $this->configFactory->getEditable('login_lock.settings')->get('status_message');

    return $status_message ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getLogoutMessage(): string {
    $logout_message = $this->configFactory->getEditable('login_lock.settings')->get('logout_message');

    return $logout_message ?? '';
  }

}
