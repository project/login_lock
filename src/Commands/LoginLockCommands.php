<?php

namespace Drupal\login_lock\Commands;

use Drupal\login_lock\LoginLockHelperInterface;
use Drush\Commands\DrushCommands;

/**
 * Login Lock commands for Drush 9+.
 */
class LoginLockCommands extends DrushCommands {

  /**
   * Login Lock helper.
   *
   * @var \Drupal\login_lock\LoginLockHelperInterface
   */
  protected $loginLockHelper;

  /**
   * LoginLockCommands constructor.
   *
   * @param \Drupal\login_lock\LoginLockHelperInterface $loginLockHelper
   *   Login Lock helper.
   */
  public function __construct(LoginLockHelperInterface $loginLockHelper) {
    parent::__construct();
    $this->loginLockHelper = $loginLockHelper;
  }

  /**
   * Enable or disable login lock.
   *
   * @param bool $status
   *   New status of login lock.
   *
   * @command login-lock-status
   *
   * @usage drush login-lock-status 1
   *   Enable login lock.
   * @usage drush login-lock-status 0
   *   Disable login lock.
   *
   * @validate-module-enabled login_lock
   */
  public function loginLockStatus($status) {
    if ($status) {
      $this->loginLockHelper->enableLock();
      $this->logger()->notice(dt('Login Lock were enabled'));
    }
    else {
      $this->loginLockHelper->disableLock();
      $this->logger()->notice(dt('Login Lock were disabled'));
    }
  }

  /**
   * Enable login lock.
   *
   * @command login-lock-enable
   *
   * @usage drush login-lock-enable
   *   Enable login lock.
   *
   * @validate-module-enabled login_lock
   */
  public function loginLockEnable() {
    $this->loginLockHelper->enableLock();
    $this->logger()->notice(dt('Login Lock were enabled'));
  }

  /**
   * Disable login lock.
   *
   * @command login-lock-disable
   *
   * @usage drush login-lock-disable
   *   Disable login lock.
   *
   * @validate-module-enabled login_lock
   */
  public function loginLockDisable() {
    $this->loginLockHelper->disableLock();
    $this->logger()->notice(dt('Login Lock were disabled'));
  }

}
