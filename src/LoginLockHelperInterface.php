<?php

namespace Drupal\login_lock;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\user\RoleInterface;

/**
 * Defines Login Lock helper interface.
 */
interface LoginLockHelperInterface {

  /**
   * Check status of login lock.
   *
   * @return bool
   *   TRUE if login lock enabled, FALSE otherwise.
   */
  public function isLoginLockEnabled(): bool;

  /**
   * Check if passed user can login even if enabled login lock.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account to check.
   *
   * @return bool
   *   TRUE if user can login, FALSE otherwise.
   */
  public function canUserBypassLoginLock(AccountInterface $account): bool;

  /**
   * Check if passed role can login even if enabled login lock.
   *
   * @param \Drupal\user\RoleInterface $role
   *   Role to check.
   *
   * @return bool
   *   TRUE if user with passed role can login, FALSE otherwise.
   */
  public function canRoleBypassLoginLock(RoleInterface $role): bool;

  /**
   * Enable login lock.
   */
  public function enableLock(): void;

  /**
   * Disable login lock.
   */
  public function disableLock(): void;

  /**
   * Get redirect URL.
   *
   * @return \Drupal\Core\Url
   *   URL object.
   */
  public function getRedirectUrl(): Url;

  /**
   * Get login message.
   *
   * @return string
   *   Login message.
   */
  public function getLoginMessage(): string;

  /**
   * Get status message.
   *
   * @return string
   *   Status message.
   */
  public function getStatusMessage(): string;

  /**
   * Get logout message.
   *
   * @return string
   *   Logout message.
   */
  public function getLogoutMessage(): string;

}
