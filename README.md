# Login Lock (login_lock)

The Login Lock (login_lock) module allows you to enforce user logout and prevent login for specific user roles during the enabled login lock.

Sometimes, when planning to deploy updates to your site, you may not want to put it in "maintenance mode" as you still want visitors to access and read the content. Instead, you can utilize this module to log out all users and disable authorization on the site during the deployment process.

## Features

- Log out all users (except those with roles able to bypass the login lock) when the Login Lock is enabled, and redirect them to a configured page.
- Display a maintenance mode message when users attempt to authorize, prompting them to try again later.

## Usage

1. Enable the module.
2. Go to the module settings page: `/admin/config/people/login-lock/settings`, and configure the module according to your requirements.
3. Access the Login Lock status form: `/admin/config/people/login-lock`, and enable the Login Lock. Alternatively, you can use the following Drush command: `drush login-lock-status 1` to enable the Login Lock.
4. As a result, users without permission to bypass the login lock will be unable to log in. They will be automatically logged out and redirected to the configured page.
5. To disable the login lock, navigate to the Login Lock status form: `/admin/config/people/login-lock`, and disable the Login Lock. Alternatively, you can use the following Drush command: `drush login-lock-status 0` to disable the Login Lock.

## Drush commands

- `drush login-lock-status 1`: Enable the login lock.
- `drush login-lock-status 0`: Disable the login lock.